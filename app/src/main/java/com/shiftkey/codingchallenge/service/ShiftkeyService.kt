package com.shiftkey.codingchallenge.service

import com.shiftkey.codingchallenge.service.model.ShiftsDataResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ShiftkeyService {

    @GET("available_shifts")
    fun getAvaiableShifts(
        @Query("address") address: String = "Dallas, TX",
        @Query("type") type: String = "list",
        @Query("start") start: String,
        @Query("end") end: String
    ): Single<ShiftsDataResponse>

}
