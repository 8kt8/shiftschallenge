package com.shiftkey.codingchallenge.service.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ShiftsDataResponse(
    @Json(name = "data")
    val data: List<ShiftsResponse>
)

@JsonClass(generateAdapter = true)
data class ShiftsResponse(
    @Json(name = "shifts")
    val shifts: List<ShiftResponse>
)

@JsonClass(generateAdapter = true)
data class ShiftResponse(
    @Json(name = "shift_id")
    val id: Int,
    @Json(name = "normalized_start_date_time")
    val startDateTime: String,
    @Json(name = "normalized_end_date_time")
    val endDateTime: String,
    @Json(name = "premium_rate")
    val premiumRate: Boolean,
    @Json(name = "covid")
    val covid: Boolean,
    @Json(name = "shift_kind")
    val shift: String,
    @Json(name = "facility_type")
    val facilityType: FacilityTypeResponse,
    @Json(name = "localized_specialty")
    val localizedSpeciality: LocalizedSpecialtyResponse,
    @Json(name = "skill")
    val skill: SkillResponse
)

@JsonClass(generateAdapter = true)
data class SkillResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "color")
    val color: String,
)

@JsonClass(generateAdapter = true)
data class FacilityTypeResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "name")
    val name: String,
    @Json(name = "color")
    val color: String,
)

@JsonClass(generateAdapter = true)
data class LocalizedSpecialtyResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "specialty_id")
    val specialityId: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "specialty")
    val speciality: SpecialtyResponse,
)

@JsonClass(generateAdapter = true)
data class SpecialtyResponse(
    @Json(name = "id")
    val id: Int,
    @Json(name = "color")
    val color: String,
    @Json(name = "name")
    val name: String,
    @Json(name = "abbreviation")
    val abbreviation: String,
)