package com.shiftkey.codingchallenge.service

import javax.inject.Inject

class BackendConfig @Inject constructor() {
    val version = "v2"
    val apiUrl = "https://staging-app.shiftkey.com/api/$version/"
    val readTimeout = 60L
    val connectionTimeout = 60L
}