package com.shiftkey.codingchallenge.core

import com.shiftkey.codingchallenge.schedulers.CoreSchedulers
import com.shiftkey.codingchallenge.service.ShiftkeyService
import com.shiftkey.codingchallenge.service.model.ShiftResponse
import com.shiftkey.codingchallenge.service.model.ShiftsDataResponse
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.Subject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShiftyRepository @Inject constructor(
    private val shiftyService: ShiftkeyService,
    private val coreSchedulers: CoreSchedulers
) {

    private val shiftsResult: Subject<ShiftsDataResponse> =
        BehaviorSubject.create<ShiftsDataResponse>().toSerialized()

    fun refresh(
        address: String,
        type: String,
        startDate: String,
        endDate: String
    ): Completable =
        shiftyService.getAvaiableShifts(
            address = address,
            type = type,
            start = startDate,
            end = endDate
        )
            .subscribeOn(coreSchedulers.networkIO)
            .doAfterSuccess(::updateData)
            .onErrorComplete()
            .ignoreElement()

    fun getShifts(): Flowable<ShiftsDataResponse> =
        shiftsResult.toFlowable(BackpressureStrategy.LATEST)

    fun getShiftById(id: Int): Flowable<ShiftResponse> =
        getShifts()
            .map { shiftsDataResponse ->
                shiftsDataResponse.data.flatMap { response -> response.shifts }
                    .find { shiftId ->
                        shiftId.id == id
                    } ?: throw NoSuchElementException()
            }


    private fun updateData(data: ShiftsDataResponse) {
        shiftsResult.onNext(data)
    }
}