package com.shiftkey.codingchallenge.common

import android.app.Application
import androidx.startup.AppInitializer
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.hilt.android.HiltAndroidApp
import net.danlew.android.joda.JodaTimeInitializer

@HiltAndroidApp
class ShiftkeyApplication: Application(){

    override fun onCreate() {
        super.onCreate()
        AppInitializer.getInstance(this).initializeComponent(JodaTimeInitializer::class.java)
        Logger.addLogAdapter(AndroidLogAdapter())
    }
}