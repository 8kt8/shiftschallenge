package com.shiftkey.codingchallenge.common.ui

interface ListItem {
    val id: Int
    val type: Int
}