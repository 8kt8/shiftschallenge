package com.shiftkey.codingchallenge.common.data

import org.joda.time.DateTime
import javax.inject.Inject

class CurrentDateProvider @Inject constructor() {

    fun now(): DateTime = DateTime.now()
}