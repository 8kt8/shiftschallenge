package com.shiftkey.codingchallenge.common.binding

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.shiftkey.codingchallenge.BR

open class BindingViewHolder<Item, Binding : ViewDataBinding>(
    val binding: Binding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Item) {
        binding.setVariable(BR.uiModel, item)
        binding.executePendingBindings()
    }
}