package com.shiftkey.codingchallenge

import com.orhanobut.logger.Logger
import com.shiftkey.codingchallenge.common.BaseViewModel
import com.shiftkey.codingchallenge.ui.RefreshShiftsForDallasUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val refreshShiftsForDallasUseCase: RefreshShiftsForDallasUseCase
): BaseViewModel(){

    fun refresh() {
        refreshShiftsForDallasUseCase.refresh().subscribe({
            Logger.d("Refresh data success")
        }, {
            Logger.d("Refresh data error ${it.localizedMessage}")
        })
    }
}