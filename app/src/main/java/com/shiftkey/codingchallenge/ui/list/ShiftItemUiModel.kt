package com.shiftkey.codingchallenge.ui.list

import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.common.ui.ListItem

data class ShiftItemUiModel(
    override val id: Int,
    val startDateTime: String,
    val endDateTime: String,
    val premiumRateInfo: String,
    val covidInfo: String
): ListItem {

    val idString = id.toString()

    override val type: Int = R.id.item

}