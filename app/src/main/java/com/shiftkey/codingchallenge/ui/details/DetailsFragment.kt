package com.shiftkey.codingchallenge.ui.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.common.viewBinding
import com.shiftkey.codingchallenge.databinding.FragmentDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment: Fragment(R.layout.fragment_details){

    private val detailsViewModel: DetailsViewModel by viewModels()

    private val binding: FragmentDetailsBinding by viewBinding(FragmentDetailsBinding::bind)

    private val args: DetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBoundView()
    }

    private fun onBoundView(){
        binding.lifecycleOwner = viewLifecycleOwner
        detailsViewModel.getById(args.id).observe(viewLifecycleOwner){
            binding.uiModel = it
        }
    }

}