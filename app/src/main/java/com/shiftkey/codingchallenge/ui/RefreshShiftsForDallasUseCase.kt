package com.shiftkey.codingchallenge.ui

import com.shiftkey.codingchallenge.common.data.CurrentDateProvider
import com.shiftkey.codingchallenge.core.ShiftyRepository
import io.reactivex.rxjava3.core.Completable
import javax.inject.Inject

class RefreshShiftsForDallasUseCase @Inject constructor(
    private val shiftyRepository: ShiftyRepository,
    private val currentDateProvider: CurrentDateProvider
) {

    fun refresh(): Completable =
        shiftyRepository.refresh(
            address = ADDRESS,
            type = TYPE,
            startDate = getStartDate(),
            endDate = getEndDate()
        )

    private fun getStartDate() = currentDateProvider.now()
        .toString(PATTERN)

    private fun getEndDate() = currentDateProvider.now()
        .plusDays(REFRESH_DAYS_RANGE)
        .toString(PATTERN)

    companion object {
        private const val ADDRESS = "Dallas, TX"
        private const val TYPE = "list"
        private const val REFRESH_DAYS_RANGE = 7
        private const val PATTERN = "YYYY-MM-dd"
    }
}