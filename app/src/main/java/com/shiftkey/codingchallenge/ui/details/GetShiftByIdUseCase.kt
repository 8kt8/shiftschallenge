package com.shiftkey.codingchallenge.ui.details

import com.shiftkey.codingchallenge.core.ShiftyRepository
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class GetShiftByIdUseCase @Inject constructor(
    private val shiftyRepository: ShiftyRepository,
    private val detailsUiModelMapper: DetailsUiModelMapper
) {

    fun get(id: Int): Flowable<DetailsUiModel> = shiftyRepository.getShiftById(id)
        .map { detailsUiModelMapper.map(it) }
}