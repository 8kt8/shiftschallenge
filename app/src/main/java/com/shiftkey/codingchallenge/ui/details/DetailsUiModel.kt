package com.shiftkey.codingchallenge.ui.details

data class DetailsUiModel(
    val facility: String,
    val skill: String,
    val skillColor: String,
    val localizedSpeciality: String,
    val speciality: String,
    val specialityAbbreviation: String,
)