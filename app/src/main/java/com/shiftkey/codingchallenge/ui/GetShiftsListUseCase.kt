package com.shiftkey.codingchallenge.ui

import com.shiftkey.codingchallenge.core.ShiftyRepository
import com.shiftkey.codingchallenge.ui.list.ShiftItemUiModel
import com.shiftkey.codingchallenge.ui.list.ShiftItemUiModelMapper
import io.reactivex.rxjava3.core.Flowable
import javax.inject.Inject

class GetShiftsListUseCase @Inject constructor(
    private val shiftyRepository: ShiftyRepository,
    private val shiftItemUiModelMapper: ShiftItemUiModelMapper
) {

    fun get(): Flowable<List<ShiftItemUiModel>> = shiftyRepository.getShifts().map {
        shiftItemUiModelMapper.map(it)
    }
}