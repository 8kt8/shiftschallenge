package com.shiftkey.codingchallenge.ui.details

import com.shiftkey.codingchallenge.service.model.ShiftResponse
import javax.inject.Inject

class DetailsUiModelMapper @Inject constructor() {

    fun map(shiftResponse: ShiftResponse) = with(shiftResponse) {
        DetailsUiModel(
            facility = facilityType.name,
            skill = skill.name,
            skillColor = skill.color,
            localizedSpeciality = localizedSpeciality.name,
            speciality = localizedSpeciality.speciality.name,
            specialityAbbreviation = localizedSpeciality.speciality.abbreviation
        )
    }
}