package com.shiftkey.codingchallenge.ui.list

import com.shiftkey.codingchallenge.service.model.ShiftsDataResponse
import com.shiftkey.codingchallenge.service.model.ShiftsResponse
import javax.inject.Inject

class ShiftItemUiModelMapper @Inject constructor() {

    fun map(shiftsResponse: ShiftsDataResponse) = shiftsResponse.data
        .flatMap { it.shifts }.map {
            ShiftItemUiModel(
                id = it.id,
                startDateTime = it.startDateTime,
                endDateTime = it.endDateTime,
                premiumRateInfo = "Premium rate: ${it.premiumRate}",
                covidInfo = "Covid: ${it.covid}"
            )
        }
}