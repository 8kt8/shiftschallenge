package com.shiftkey.codingchallenge.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams.fromPublisher
import com.shiftkey.codingchallenge.common.BaseViewModel
import com.shiftkey.codingchallenge.ui.GetShiftsListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    getShiftsListUseCase: GetShiftsListUseCase
) : BaseViewModel() {

    init {
        setLoading(true)
    }

    val shiftItems: LiveData<List<ShiftItemUiModel>> =
       fromPublisher(getShiftsListUseCase.get()
           .doOnEach { setLoading(false) }
           .doOnError { triggerError(it) }
       )

}