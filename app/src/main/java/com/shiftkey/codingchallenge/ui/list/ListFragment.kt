package com.shiftkey.codingchallenge.ui.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.giphyapp.common.utils.showToastLong
import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.common.viewBinding
import com.shiftkey.codingchallenge.databinding.FragmentListBinding
import com.shiftkey.codingchallenge.databinding.ItemBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListFragment : Fragment(R.layout.fragment_list) {

    private val listViewModel: ListViewModel by viewModels()

    private val binding: FragmentListBinding by viewBinding(FragmentListBinding::bind)

    private val listAdapter: ListAdapter by lazy { ListAdapter(::onItemClicked) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBoundView()
        observeApiError()
    }

    private fun onBoundView() =
        with(binding) {
            lifecycleOwner = viewLifecycleOwner
            adapter = listAdapter
            viewModel = listViewModel
        }

    private fun onItemClicked(shiftItemUiModel: ShiftItemUiModel, binding: ItemBinding) {
        val direction = ListFragmentDirections.actionToDetails(shiftItemUiModel.id)
        findNavController().navigate(direction)
    }

    private fun observeApiError() {
        listViewModel.errorEvent.observe(viewLifecycleOwner) {
            if (listAdapter.itemCount == 0) {
                showToastLong(it.localizedMessage ?: "Api error")
            }
        }
    }

}