package com.shiftkey.codingchallenge.ui.details

import androidx.lifecycle.LiveDataReactiveStreams.fromPublisher
import com.shiftkey.codingchallenge.common.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val getShiftByIdUseCase: GetShiftByIdUseCase
): BaseViewModel() {

    fun getById(id: Int) = fromPublisher(getShiftByIdUseCase.get(id))
}