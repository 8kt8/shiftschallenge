package com.shiftkey.codingchallenge.ui.list

import com.shiftkey.codingchallenge.R
import com.shiftkey.codingchallenge.common.binding.BindingListAdapter
import com.shiftkey.codingchallenge.databinding.ItemBinding

class ListAdapter(onItemClickListener: (ShiftItemUiModel, ItemBinding) -> Unit) :
    BindingListAdapter<ShiftItemUiModel, ItemBinding>(onItemClickListener) {

    override fun getLayoutId(viewType: Int): Int = R.layout.item

}